*** Settings ***
Documentation     Robot creado con robocorp - RPA Challenge !
Library           RPA.Browser.Selenium
Library           RPA.Excel.Files


*** Variables ***
${URL}=    http://www.rpachallenge.com/
${SELECTOR_button_start}=   //button[contains(text(),'Start')]
${SELECTOR_button_submit}=  //body/app-root[1]/div[2]/app-rpa1[1]/div[1]/div[2]/form[1]/input[1]
${SELECTOR_input_role_in_company}=  //label[contains(text(),'Role in Company')]/following::input[1]
${SELECTOR_input_first_name}=   //label[contains(text(),'First Name')]/following::input[1]
${SELECTOR_input_phone_number}=   //label[contains(text(),'Phone Number')]/following::input[1]
${SELECTOR_input_company_name}=     //label[contains(text(),'Company Name')]/following::input[1]
${SELECTOR_input_address}=  //label[contains(text(),'Address')]/following::input[1]
${SELECTOR_input_email}=    //label[contains(text(),'Email')]/following::input[1]
${SELECTOR_input_last_name}=    //label[contains(text(),'Last Name')]/following::input[1]


*** Keywords ***
Open rpa challenge
    Open Available Browser    ${URL}

*** Keywords ***
Web Challenge start
    Click Button    xpath:${SELECTOR_button_start}

*** Keywords ***
Ingresar datos para un registro
    [arguments]  ${registro}
    Input Text  xpath:${SELECTOR_input_role_in_company}     ${registro}[Role in Company]
    Input Text  xpath:${SELECTOR_input_first_name}          ${registro}[First Name]
    Input Text  xpath:${SELECTOR_input_phone_number}        ${registro}[Phone Number]
    Input Text  xpath:${SELECTOR_input_company_name}        ${registro}[Company Name]
    Input Text  xpath:${SELECTOR_input_address}             ${registro}[Address]
    Input Text  xpath:${SELECTOR_input_email}               ${registro}[Email]
    Input Text  xpath:${SELECTOR_input_last_name}           ${registro}[Last Name]
    Click Button    xpath:${SELECTOR_button_submit}
    

*** Keywords ***
Ingreso de registros
    Open Workbook    challenge.xlsx
    ${registros}=    Read Worksheet As Table    header=True
    Close Workbook
    
    FOR    ${registro}    IN    @{registros}
        Ingresar datos para un registro     ${registro}
    END
    

*** Tasks ***
Challege RPA Task
    Open rpa challenge
    Web Challenge start
    Ingreso de registros
    Sleep       10000ms


